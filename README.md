# README #

Bootstrap a fresh debian:

    apt-get update && apt-get install -y ca-certificates && wget -qO- https://bitbucket.org/gpiffault/dotfiles/raw/master/debian-bootstrap.sh | sh

Get the osx compatible red prompt:

    wget -O .bashrc https://bitbucket.org/gpiffault/dotfiles/raw/master/.bashrc-red-prompt
