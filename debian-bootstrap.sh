apt-get update
apt-get install -y sudo nano ntp ufw unattended-upgrades

ufw allow 22
ufw --force enable

# cat << 'EOF' > /etc/apt/apt.conf.d/10periodic
# APT::Periodic::Update-Package-Lists "1";
# APT::Periodic::Download-Upgradeable-Packages "1";
# APT::Periodic::AutocleanInterval "7";
# APT::Periodic::Unattended-Upgrade "1";
# EOF

cat << 'EOF' > /etc/apt/apt.conf.d/20auto-upgrades
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
EOF

# old_name=`hostname`
# old_prefix=`hostname -s`
# echo "Current host name: $old_name"
# read -p "New host name (leave blank to keep current name): " new_name
# if [[ $new_name =~ ^(.+)\.[^.]+\.[^.]+$ ]]; then
# 	new_prefix=${BASH_REMATCH[1]}
# 	cp /etc/hostname /etc/hostname.bak
# 	sed -i "s/$old_name/$new_name/g" /etc/hostname
# 	cp /etc/hosts /etc/hosts.bak
# 	sed -i "s/$old_name/$new_name/g" /etc/hosts
# 	sed -i "s/$old_prefix/$new_prefix/g" /etc/hosts
#   echo "Reboot to make new name active"
# else
#   echo "Name not changed"
# fi

echo "edit host names in /etc/hostname and /etc/hosts"

# Disable OVH kernel
# apt-get install linux-image-generic
# mv /etc/grub.d/06_OVHkernel /etc/grub.d/11_OVHkernel
# grub-mkconfig
# update-grub2

adduser -gecos "" greg
adduser greg sudo
usermod -p '!' root

su greg << EOSU
cd
# wget -O .bashrc https://bitbucket.org/gpiffault/dotfiles/raw/master/.bashrc-red-prompt
mkdir .ssh
wget -O .ssh/authorized_keys https://bitbucket.org/gpiffault/dotfiles/raw/master/id_rsa.pub
EOSU
